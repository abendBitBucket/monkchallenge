from PIL import Image


class Filters:
    """Applies filters to a given image."""
    def __init__(self, input_image_path):
        """Generator receiving source image path."""
        self.input_image_path = input_image_path
        self.image = None

    def __enter__(self):
        """Magic method for the using of 'with'."""
        self.image = Image.open(self.input_image_path)

        return self

    def __exit__(self, exception_type, exception_value, traceback):
        """Magic method for the using of 'with'."""
        self.image.close()

    def gray_scale(self):
        """Converts the image to black and white."""
        self.image = self.image.convert('L').convert(self.image.mode)

        return self

    def overlay(self, overlay_image_path):
        """Overlay a given image on top of the source."""
        with Image.open(overlay_image_path).convert('RGBA') as im:
            self.image.paste(im, (0, 0), im)

        return self

    def rotate(self, degrees):
        """Rotate N degrees anticlockwise."""
        self.image = self.image.rotate(degrees)

        return self

    def save(self, output_image):
        """Generates a new edited image from source."""
        self.image.save(output_image)


