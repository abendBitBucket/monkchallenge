import argparse
import sys
from settings import dict_parameters_map, dict_formats_allowed
from image_processing import filters


def get_params_and_filtering():
    """Returns positional arguments and a list with filters to apply."""
    dict_arg_parse = parse_arguments()

    filtering_order = []

    for arg in sys.argv:
        try:
            filtering_order.append(
                (
                    dict_parameters_map[arg],
                    dict_arg_parse[dict_parameters_map[arg]].pop(0)[0]
                )
            )
        except KeyError:
            pass

    return dict_arg_parse['source_image'], dict_arg_parse['target_image'], filtering_order


def parse_arguments():
    """Validates and parses arguments received from command line and returns a dictionary."""
    parser = argparse.ArgumentParser(description="applies filters to an image and saves it as a new file")
    parser.add_argument('source_image',
                        help='source image to be processed')
    parser.add_argument('target_image',
                        help='target image to be processed')
    parser.add_argument('-gs', '--gray_scale',
                        action='append_const',
                        const=[True],
                        help='convert the image to black and white')
    parser.add_argument('-ov', '--overlay',
                        action='append',
                        metavar='overlay_image',
                        nargs=1,
                        help='overlay a given image on top of the source')
    parser.add_argument('-rt', '--rotate',
                        action='append',
                        nargs=1,
                        metavar='degrees',
                        type=int,
                        help='rotate N degrees')

    dict_parser = vars(parser.parse_args())

    if dict_parser['target_image'].split('.')[-1] not in dict_formats_allowed['target_image']:
        parser.error('not valid target image format: ' + dict_parser['target_image'])
    else:
        not_valid_overlay = [x[0] for x in dict_parser['overlay']
                             if x[0].split('.')[-1] not in dict_formats_allowed['overlay']]

        if not_valid_overlay:
            parser.error('not valid overlay image format: ' + str(not_valid_overlay))

    return dict_parser


if __name__ == "__main__":

    source_image, target_image, filtering = get_params_and_filtering()

    with filters.Filters(source_image) as image:
        for filter_to_apply in filtering:
            if filter_to_apply[0] == 'overlay':
                image.overlay(filter_to_apply[1])
            elif filter_to_apply[0] == 'rotate':
                image.rotate(filter_to_apply[1])
            elif filter_to_apply[0] == 'gray_scale':
                image.gray_scale()

        image.save(target_image)
