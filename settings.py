from enum import Enum


class FiltersEnum(Enum):
    OVERLAY = 'overlay'
    GRAY_SCALE = 'gray_scale'
    ROTATE = 'rotate'


dict_parameters_map = {
                   '-ov': FiltersEnum.OVERLAY.value,
                   '--overlay': FiltersEnum.OVERLAY.value,
                   '-gs': FiltersEnum.GRAY_SCALE.value,
                   '--gray_scale': FiltersEnum.GRAY_SCALE.value,
                   '-rt': FiltersEnum.ROTATE.value,
                   '--rotate': FiltersEnum.ROTATE.value,
}

dict_formats_allowed = {
                   'overlay': {'png'},
                   'target_image': {'png', 'jpg'},
}
