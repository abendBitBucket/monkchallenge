### Python coding challenge: Quick Start Guide



#### What we have here

* **main.py**: the main app we need to run with its respective parameters.
* **settings.py**: configuration module to gets everything tidy.
* **image_processing**: package to put any reusable custom module related to image treatment .
  * **filters.py**: module for image filtering

#### Usage

To run it, you need to pass several parameters:

    python main.py source_image target_image [-h] [-gs] [-ov overlay_image] [-rt degrees]


Required arguments:

* **source_image**: path to input image.
* **target_image**: path to output image. Valid formats are jpg and png.

Optional arguments:

* **-h**, **--help**: shows help message
* **-gs**, **--gray_scale**: converts source image to black and white
* **-ov *overlay_image***, **--overlay *overlay_image***: overlay the image at *overlay_image* location on top of the source. Valid format: png
* **-rt *degrees***, **--rotate *degrees***: rotate the image *deegres* degrees anticlockwise

**Note:** filter parameters can be provided in any order and can be repeated.


##### Example

    python main.py input.jpg output.jpg -gs -ov python.png -rt 180 -gs -ov python.png
    
It applies filters in the following way: gray scale -> overlay -> rotate -> gray scale -> overlay

Source image:

![Source image](https://i.imgur.com/6ppt7Jc.jpg)

Target image:

![Target image](https://i.imgur.com/wUbLzJI.jpg)


#### Steps to add new filters:

1. Add a new method in filters.py for the new functionality, you can take existing ones as examples.
1. To add new parameters to be accepted by the application you have to add them to the dictionary 'dict_parameters_map' in settings.py.
1. In parse_arguments() function in main.py, you have to add the parser's new rules and validations for the new parameter.


##### Time spent to it:

It was about 6 hours. I had to get into Pillow library and in some way to argparse built-in library. I was not used to image processing and to achieve the command line part as requested I had to investigate a little.
